package eg.edu.alexu.csd.datastructure.maze.cs24;


public class MyLinkedList {

	private Node head = null;
	private Node tail = null;
	private int size=0;

	public void add(int r,int c,int pR,int pC) {
		

		Node newNode = new Node();
			
		newNode.row = r;
		newNode.columne = c;
		newNode.parentRow = pR;
		newNode.parentColumne = pC;
			
		if(head == null){
				head=newNode;
			}
			else{
				if(tail == null){
					head.next=newNode;
					newNode.pre=head;
					
					tail=newNode;
				}
				else{
			
			
			
			tail.next=newNode;
		
			newNode.pre=tail;
			tail=newNode;
			}
			}
			size++;
	}

	public int[] getfirst() {
		
		int[] values = {head.row,head.columne};
		
		if (isEmpty()){
			 return null;
		 }
		
		else{
		return values;
		}
	}
	
	public int[] get(int r,int c) {
		
		Node i = head;
		while(i != null){
			if((i.row == r) && (i.columne == c)){
				break;
			}
			i=i.next;
		}
		
		int[] values = {i.parentRow,i.parentColumne};
		
		return values;
		
	}
	
	
	public boolean isEmpty() {
		
		if (head == null) {
			return true;
		}
		return false;
	}
	
	public void removefirst() {

		if (isEmpty()){
			throw null;
		}
		else if(head.next == null){
			head = null;
			tail = null;
		}
		else if(head.next.next == null){
			head = tail;
			head.pre = null;
			tail = null;
		}
		else{
			head = head.next;
			head.pre = null;
		}
		size--;
	}

	public int size() {
		
			return size;
		
	}
}
