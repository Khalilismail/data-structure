package eg.edu.alexu.csd.datastructure.maze.cs24;

import java.awt.Point;
import java.io.File;
import java.util.Scanner;

import eg.edu.alexu.csd.datastructure.maze.IMazeSolver;

public class MazeImplementation implements IMazeSolver {
	
	
	
	
	private Scanner x ;
	
	private String rowDimention = "";
	private String columneDimention = "";
	private int n,m;
	private String[] mapAsString = new String[100];
	private Object[][] map;
	
	private Queue q = new Queue();
	private Queue q1 = new Queue();
	
	
	private boolean[][] visitedMap;
	private int[][] path = new int[100][2];;
	private int[][] realPath;
	private int pointCounter = 0;
	
	
	
	
	public void readFile(File maze){

		try{
			x = new Scanner(maze);
		}
		catch(Exception e){
			throw null;
		}
		
		rowDimention = x.next();
		
		columneDimention = x.next();
				
		
		n = (int)(rowDimention.charAt(0))  - 48;
		
		m = (int)(columneDimention.charAt(0)) - 48;
		
		map = new Object[n][m];
		
		
		int counter = 0;
		while(x.hasNext()){
			
			mapAsString[counter] = x.next();
			counter++;
		}
		
		if(counter == 0 || counter != n || mapAsString[0].length() != m){
			throw null;
		}
		
		char[][] z = new char[n][m];
		
		for(int i = 0; i < n; i++){
			
			z[i] = mapAsString[i].toCharArray();
			
		}
		
		for(int i = 0; i < n; i++){
			for(int j = 0; j < m; j++){
				map[i][j] = z[i][j];
			}
		}
	
	}

	
	
	public void addToBathUsingDfs(int r,int c){
		
		for(int i=0;i<n;i++){
			for(int j=0;j<m;j++){
				if(i==r && j==c){
				if (visitedMap[i][j]!=true){
					visitedMap[i][j]=true;
					path[pointCounter][0] = i;
					path[pointCounter][1] = j;
					pointCounter++;
					
					if((char)map[i][j] == 'E'){
						for(int k=0;k<n;k++){
							for(int f=0;f<m;f++){
								
								visitedMap[k][f]=true;
								
								
							}
						}
						
					}
					else{
					if((i<n-1) && ((char)map[i+1][j]!='#')){
						addToBathUsingDfs(i+1,j);
					}
					if((i>0) && ((char)map[i-1][j]!='#')){
						addToBathUsingDfs(i-11,j);
					}
					if((j>0) && ((char)map[i][j-1]!='#')){
						addToBathUsingDfs(i,j-1);
					}
					if((j<m-1) && ((char)map[i][j+1]!='#')){
						addToBathUsingDfs(i,j+1);
					}
					}
					
				}
				else{
					break;
			 }
			}
		  }
			
		}
		
	}


	@Override
	public int[][] solveBFS(File maze) {

		// TODO Auto-generated method stub
		readFile(maze);
		int[] values ;
		int r=0;
		int c=0;
		int flag = 0;
		visitedMap = new boolean[n][m];
		
		for(int i = 0; i < n; i++){
			for(int j = 0; j < m; j++){
				if((char)map[i][j]=='S'){
					q.enqueue(i, j, -1, -1);
					q1.enqueue(i, j, -1, -1);
					visitedMap[i][j] = true;
					break;
				}
			}
		}
		
		while(!q.isEmpty()){
			
			values = q.dequeue();
			
			
			visitedMap[values[0]][values[1]] = true;
			
			if((char)map[values[0]][values[1]]=='E'){
				r = values[0];
				c = values[1];
				flag = 1;
				break;
			}
			
			if((values[0] > 0) && ((char)map[values[0]-1][values[1]] != '#') && (visitedMap[values[0]-1][values[1]]) == false){
				q.enqueue(values[0]-1,values[1],values[0],values[1]);
				q1.enqueue(values[0]-1,values[1],values[0],values[1]);
			}
			
			if((values[1] > 0) && ((char)map[values[0]][values[1]-1] != '#') && (visitedMap[values[0]][values[1]-1]) == false){
				q.enqueue(values[0],values[1]-1,values[0],values[1]);
				q1.enqueue(values[0],values[1]-1,values[0],values[1]);
			}
			
			if((values[0] < n-1) && ((char)map[values[0]+1][values[1]] != '#') && (visitedMap[values[0]+1][values[1]]) == false){
				q.enqueue(values[0]+1,values[1],values[0],values[1]);
				q1.enqueue(values[0]+1,values[1],values[0],values[1]);
			}
			
			if((values[1] < m-1) && ((char)map[values[0]][values[1]+1] != '#') && (visitedMap[values[0]][values[1]+1]) == false){
				q.enqueue(values[0],values[1]+1,values[0],values[1]);
				q1.enqueue(values[0],values[1]+1,values[0],values[1]);
			}
			
		}
		if(flag == 0){
			throw null;
		}
		path[pointCounter][0] = r;
		path[pointCounter][1] = c;
		pointCounter++;
		
		values = q1.getParent(r, c);
		while((values[0] != -1) && (values[1] != -1)){
			r = values[0];
			c = values[1];
			path[pointCounter][0] = r;
			path[pointCounter][1] = c;
			pointCounter++;
			
			values = q1.getParent(r, c);
		}
		
		realPath = new int[pointCounter][2];
		int tmp = 0;
		for(int i = pointCounter-1; i >= 0; i--){
			for(int j = 0; j < 2; j++){
				realPath[tmp][j] = path[i][j];
			}
			tmp++;
		}
		
		return realPath;
	}

	@Override
	public int[][] solveDFS(File maze) {
		// TODO Auto-generated method stub
		readFile(maze);
		
		int flag = 0;
		visitedMap = new boolean[n][m];
		for(int i = 0; i < n; i++){
			for(int j = 0; j < m; j++){
				if((char)map[i][j]=='S'){
					addToBathUsingDfs(i,j);
					break;
				}
			}
		}
		for(int i = 0; i < n; i++){
			for(int j = 0; j < m; j++){
				if(visitedMap[i][j]==false){
					flag=1;
				}
			}
		
		}
		realPath = new int[pointCounter][2];
		for(int i = 0; i < pointCounter; i++){
			for(int j = 0; j < 2; j++){
				realPath[i][j] = path[i][j];
			}
		}
		if(flag==1){
			throw null;
		}
		else{
		return realPath;
		}
	}
	
	

}
