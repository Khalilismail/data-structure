package eg.edu.alexu.csd.datastructure.maze.cs24;


public class Queue {

	RuntimeException e = new RuntimeException();
	
	private MyLinkedList l = new MyLinkedList();
	
	public void enqueue(int r,int c,int pR,int pC) {
		
		l.add(r,c,pR,pC);
	}
	
	public int[] dequeue() {
		int[] values;
		
		if(l.size() > 0){
			values = l.getfirst();
			l.removefirst();
			
			return values;
		}
		else {
			throw e;
		}
	}
	
	public int[] getParent(int r,int c) {
		return l.get(r,c);
	}
	
	
	public boolean isEmpty() {
		
		if(l.isEmpty()){
			return true; 
		}
		else{
		return false;
		}
	}
	
	public int size() {
	
		return l.size();
	}
	
	
}
