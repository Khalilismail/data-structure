package eg.edu.alexu.csd.datastructure.maze.cs24;


public class Node {

	public int  row = -1;
	public int  columne = -1;
	public int  parentRow = -1;
	public int  parentColumne = -1;
	public Node next = null ;
	public Node pre = null ;
}
