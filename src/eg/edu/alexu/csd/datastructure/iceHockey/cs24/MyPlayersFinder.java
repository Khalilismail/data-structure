package eg.edu.alexu.csd.datastructure.iceHockey.cs24;



import java.awt.Point;

import eg.edu.alexu.csd.datastructure.iceHockey.IPlayersFinder;

public class MyPlayersFinder implements IPlayersFinder {

	private Point[] players=new Point[100];
	
	public void mark(String[] x,boolean[][] y,int m,int f,String v){
		
		for(int i=0;i<y.length;i++){
			for(int j=0;j<y[i].length;j++){
				if(i==m && j==f){
				if (x[i].charAt(j)==v.charAt(0) && y[i][j]==false){
					y[i][j]=true;
					
					
				if(i>0){
					mark(x,y,i-1,j,v);
				}
				if(j>0){
					mark(x,y,i,j-1,v);
				}
				if(i<y.length-1){
					mark(x,y,i+1,j,v);
				}
				if(j<y[i].length-1){
					mark(x,y,i,j+1,v);
				}
				
			}
				else{
					break;
			 }
			}
		  }
		}
	
	  }
	
	
	public int count(boolean[][] x,boolean[][] y){

		int c=0;
		for(int i=0;i<y.length;i++){
			for(int j=0;j<y[i].length;j++){
				if(x[i][j]==true && y[i][j]==false){
					c++;
					y[i][j]=true;
				}
			}
			}
		
		
		return c;
	}
	


	
	@Override
	public Point[] findPlayers(String[] photo, int team, int threshold) {
		
		
		boolean[][] Iphoto=new boolean[photo.length][photo[0].length()];
        boolean[][] visited=new boolean[photo.length][photo[0].length()];
		
	
		
		
	String x ="";
	x = Integer.toString(team);
	int f=0;
	for(int i=0;i<photo.length;i++){
		for(int j=0;j<photo[i].length();j++){
			
			
			if(photo[i].charAt(j)==x.charAt(0) && Iphoto[i][j]==false){
				int imin=0,jmin=0,imax=0,jmax=0;
				int tmp1,tmp2,tmp3,tmp4;
				
				mark(photo,Iphoto,i,j,x);
				tmp1=tmp3=i;
				tmp2=tmp4=j;
				imin=tmp1;imax=tmp3;jmin=tmp2;jmax=tmp4;
	for(int k=0;k<Iphoto.length;k++){
		for(int d=0;d<Iphoto[0].length;d++){
		
			if(Iphoto[k][d]==true && visited[k][d]==false){
				
				
				if(tmp1>k){
					imin=k;
					tmp1=imin;
				}
				if(tmp3<k){
					imax=k;
					tmp3=imax;
				}
				if(tmp2>d){
					jmin=d;
					tmp2=jmin;
				}
				if(tmp4<d){
					jmax=d;
					tmp4=jmax;
				}
				}
			
		}
		}
				
			int y =	count(Iphoto,visited);
			if(y*4 >= threshold){
				
				players[f] = new Point();
			
		
				int xav,yav;
				xav=jmin+jmax+1;
				yav=imin+imax+1;
				players[f].x=xav;
				players[f].y=yav;
				f++;
			
   }
		}
		}
				
	}
	for(int j=0;j<f;j++){
	int tmpx=players[j].x;
	int tmpy=players[j].y;
	for(int i=j+1;i<f;i++){
		
		if (tmpx>players[i].x){
			
			
			players[j].x=players[i].x;
			players[j].y=players[i].y;
			players[i].x=tmpx;
			players[i].y=tmpy;
			tmpx=players[j].x;
			tmpy=players[j].y;
		}
		if(tmpx==players[i].x){
			if(tmpy>players[i].y){
				players[j].x=players[i].x;
				players[j].y=players[i].y;
				players[i].x=tmpx;
				players[i].y=tmpy;
				tmpx=players[j].x;
				tmpy=players[j].y;
			}
		}
		
		
	}	
	
	}
	if(f>0){
	  Point[] player=new Point[f];
	   
	    player[f-1] = new Point();
	    
	   for(int i=0;i<f;i++){
	    player[i]=players[i];
	   }
	   if(f>0){
	for(int i=0;i<f;i++){
		System.out.print("(");
		System.out.print( player[i].x);
		System.out.print(",");
		System.out.print( player[i].y);
		System.out.println(")");
	}
	   }
	
		
		
		return player;
	}
	else{
		Point[] player1=new Point[0];
		 
		 return player1;
	}
	}
}
