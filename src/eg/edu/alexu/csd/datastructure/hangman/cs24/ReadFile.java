package eg.edu.alexu.csd.datastructure.hangman.cs24;
import java.io.*;
import java.util.*;

import java.lang.*;

public class ReadFile {

	public String word[]=new String[60];

	private Scanner x ;
	
	public void openFile(){
		try{
	x = new Scanner(new File("hangman dictionary.txt"));
		}
		catch(Exception e){
			System.out.println("error");
		}
	}
	public int counter=0;
	public void readFile(){
		while (x.hasNext()){
			
			word[counter] = x.next();
			
			counter++;
			}
	}
	
	public void printFile(){
		for(int i=0;i<counter;i++){
			System.out.println(word[i]);
		}
	}
	
	public void closeFile(){
		x.close();
	}

}
