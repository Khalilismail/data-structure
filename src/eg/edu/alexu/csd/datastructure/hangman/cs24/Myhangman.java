package eg.edu.alexu.csd.datastructure.hangman.cs24;

import java.util.Random;
import java.util.Scanner;

import eg.edu.alexu.csd.datastructure.hangman.IHangman;

public class Myhangman implements IHangman {
	
	public String name,name1;
	
    public int  h,n;
    public int f1=0;
    public int t=1;
    public char v;
    public String upchar,lowchar;
    public int maxi;
    public int wrong=0;
	private String[] word=new String[60];
	public int[] iword=new int[60];
	
	Scanner x = new Scanner(System.in);

	@Override
	public void setDictionary(String[] words) {
		// TODO Auto-generated method stub
	
		h=words.length;
		if(words != null){
		for(int i=0;i<words.length;i++){
			word[i]=words[i];
		}
		}
	}

	@Override
	public String selectRandomSecretWord() {
		// TODO Auto-generated method stub
		Random m = new Random();
		String d;
		n=h-1;
		if(n>0){
		int number = m.nextInt(n);
		d = word[number];
		return d;
		}
		else {
			d =word[0];
			return d;
		}
	}
	
	 
	
	
	
	
	

	
	@Override
	public String guess(Character c) {
		// TODO Auto-generated method stub
		if(f1==1){
			return null;
		}
		else{
		StringBuilder name2=new StringBuilder();
		while(t==1){
			name1 = selectRandomSecretWord();
			 
			name = name1.toUpperCase();
			
			setMaxWrongGuesses(5);
			t=0;
			
			
			
		}
		
		lowchar=Character.toString(c);
		upchar = lowchar.toUpperCase();
		v = upchar.charAt(0);
	
		
		int flag1=0,flag2=0,flag3=0;
		
	   
		
		for(int i=0;i<name.length();i++){
			if(v==name.charAt(i)){
				iword[i]=1;
				flag1=1;
			}
			if(flag1==0 && i==name.length()-1){
				flag2=1;
			}
			}
		
		
		for(int i=0;i<name.length();i++){
			if(iword[i]==1){
			name2.append(name.charAt(i));
			}
			else{
	        name2.append('-');
			}
		}
		
		
		if(flag2==1){
			wrong++;
		}
		
		
		if(wrong==maxi){
		
			
			
			return null;
		}
		else{
		
		
		
		for(int i=0;i<name.length();i++){
			if(iword[i]!=1){
				flag3=1;
			}
		}
		if(flag3==0){
			f1 = 1;
			return name;
		}
		
		else{
			return String.valueOf(name2);
		}
		
		
		
		}
	}
	}
	@Override
	public void setMaxWrongGuesses(Integer max) {
		// TODO Auto-generated method stub
	
		maxi=max;
	}

	

}
