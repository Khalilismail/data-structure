package eg.edu.alexu.csd.datastructure.queue;

public interface ILinkedList {
	

	/** Inserts the specified element at the end of the list. */
	public void add(Object element);

	/** Returns the element at the head of the list. */
	public Object getfirst();
	
	/** Returns true if this list contains no elements. */
	public boolean isEmpty();

	/** Removes the element at the head of the list. */
	public void removefirst();

	/** Returns the number of elements in this list. */
	public int size();

	
	
}
