package eg.edu.alexu.csd.datastructure.queue.cs24;

import eg.edu.alexu.csd.datastructure.queue.ILinkedBased;

import eg.edu.alexu.csd.datastructure.queue.IQueue;

	
public class MyLinkedBased implements ILinkedBased, IQueue {

	RuntimeException e = new RuntimeException();
	private Linked h = new Linked();
	@Override
	public void enqueue(Object item) {
		// TODO Auto-generated method stub

		h.add(item);
	}

	@Override
	public Object dequeue() {
		// TODO Auto-generated method stub
		Object r ;
		if(h.size() > 0){
			r = h.getfirst();
			h.removefirst();
			return r;
			
		}
		else {
			throw e;
		}
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if(h.isEmpty()){
			return true; 
		}
		else{
		return false;
		}
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return h.size();
	}

}
