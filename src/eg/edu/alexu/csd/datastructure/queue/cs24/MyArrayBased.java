package eg.edu.alexu.csd.datastructure.queue.cs24;

import eg.edu.alexu.csd.datastructure.queue.IArrayBased;
import eg.edu.alexu.csd.datastructure.queue.IQueue;

public class MyArrayBased implements IArrayBased, IQueue {

	private int f=-1;
	private int r=-1;
	private int size = 0;
	private Object[] arr;
	
	public MyArrayBased(int n){
	
	 arr = new Object[n];
	}
	
	RuntimeException e = new RuntimeException();
	
	
	@Override
	public void enqueue(Object item) {
		// TODO Auto-generated method stub

	
		if(r<arr.length-1){
		r++;
		arr[r]=item;
		size++;
		}
		else{
			throw e;
		}
	}

	@Override
	public Object dequeue() {
		// TODO Auto-generated method stub
		if(!isEmpty()){
		f++;
		size--;
		return arr[f];
		
		}
		else {
			throw e;
		}
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return (f==r);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub

		return size;
		
	}

}
