package eg.edu.alexu.csd.datastructure.queue.cs24;

import eg.edu.alexu.csd.datastructure.queue.ILinkedList;

public class Linked implements ILinkedList {

	doubleListNode head = null;
	doubleListNode tail = null;
	private int size=0;

	
	@Override
	public void add(Object element) {
		// TODO Auto-generated method stub

		if (element == null){
			throw null;
		}
		

			doubleListNode newNode = new doubleListNode();
			
			newNode.value=element;
			
			
			if(head == null){
				head=newNode;
			}
			else{
				if(tail == null){
					head.next=newNode;
					newNode.pre=head;
					
					tail=newNode;
				}
				else{
			
			
			
			tail.next=newNode;
		
			newNode.pre=tail;
			tail=newNode;
			}
			}
			size++;
	}

	
	@Override
	public Object getfirst() {
		// TODO Auto-generated method stub
		if (isEmpty()){
			 return null;
		 }
		
		else{
		return head.value;
		}
	}

	
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if (head == null) {
			return true;
		}
		return false;
	}

	@Override
	public void removefirst() {
		// TODO Auto-generated method stub

		if (isEmpty()){
			throw null;
		}
		else if(head.next == null){
			head = null;
			tail = null;
		}
		else if(head.next.next == null){
			head = tail;
			head.pre = null;
			tail = null;
		}
		else{
			head = head.next;
			head.pre = null;
		}
		size--;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		
			return size;
		
	}

	

	
}
