package eg.edu.alexu.csd.datastructure.queue.cs24;

import static org.junit.Assert.*;

public class Test {

	MyArrayBased testobject1 = new MyArrayBased(10);
	MyLinkedBased testobject2 = new MyLinkedBased();
	
	@org.junit.Test
	public void test1() {
		
		testobject1.enqueue(3);
		testobject1.enqueue(4);
		
		boolean expectedoutput1 = false;
		boolean actualoutput1 = (boolean) testobject1.isEmpty();
		assertEquals(expectedoutput1 , actualoutput1 );
		
		testobject1.dequeue();
		testobject1.dequeue();
		
		boolean expectedoutput2 = true;
		boolean actualoutput2 = (boolean) testobject1.isEmpty();
		assertEquals(expectedoutput2 , actualoutput2 );
		
	}
	
	@org.junit.Test
	public void test2() {
		int[] a={4,5,8,7,9};
		for (int i=0;i<5;i++){
			testobject1.enqueue(a[i]);
		}
		
		int expectedoutput1 = 4;
		int actualoutput1 = (int) testobject1.dequeue();
		assertEquals(expectedoutput1 , actualoutput1 );
		
		testobject1.dequeue();
		
		int expectedoutput2 = 8;
		int actualoutput2 = (int) testobject1.dequeue();
		assertEquals(expectedoutput2 , actualoutput2 );
		
	}
	
	@org.junit.Test
	public void test3() {
		int[] a={4,5,8,7,9};
		for (int i=0;i<5;i++){
			testobject1.enqueue(a[i]);
		}
		int expectedoutput = 5;
		int actualoutput = (int) testobject1.size();
		assertEquals(expectedoutput , actualoutput );
		
	}
	
	@org.junit.Test
	public void test4() {
		
		testobject2.enqueue(3);
		testobject2.enqueue(4);
		
		boolean expectedoutput1 = false;
		boolean actualoutput1 = (boolean) testobject2.isEmpty();
		assertEquals(expectedoutput1 , actualoutput1 );
		
		testobject2.dequeue();
		testobject2.dequeue();
		
		boolean expectedoutput2 = true;
		boolean actualoutput2 = (boolean) testobject2.isEmpty();
		assertEquals(expectedoutput2 , actualoutput2 );
		
	}
	
	@org.junit.Test
	public void test5() {
		int[] a={4,5,8,7,9};
		for (int i=0;i<5;i++){
			testobject2.enqueue(a[i]);
		}
		
		int expectedoutput1 = 4;
		int actualoutput1 = (int) testobject2.dequeue();
		assertEquals(expectedoutput1 , actualoutput1 );
		
		testobject2.dequeue();
		
		int expectedoutput2 = 8;
		int actualoutput2 = (int) testobject2.dequeue();
		assertEquals(expectedoutput2 , actualoutput2 );
		
	}
	
	@org.junit.Test
	public void test6() {
		int[] a={4,5,8,7,9};
		for (int i=0;i<5;i++){
			testobject2.enqueue(a[i]);
		}
		int expectedoutput = 5;
		int actualoutput = (int) testobject2.size();
		assertEquals(expectedoutput , actualoutput );
		
	}

}
