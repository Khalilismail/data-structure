package eg.edu.alexu.csd.datastructure.stack.cs24;


import eg.edu.alexu.csd.datastructure.stack.ILinkedList;

public class Linked implements ILinkedList {

	doubleListNode head = null;
	doubleListNode tail = null;
	private int size=0;
	
	@Override
	public void add(int index, Object element) {
		// TODO Auto-generated method stub

		
		if ((index < 0 )||(index > size())||(element == null)){
			throw null ;
		}
		
			doubleListNode newNode = new doubleListNode();
		doubleListNode i = head;
		newNode.value = element;
		if (index == 0) {

			newNode.next=head;
			
			head = newNode;
			
		} 
		else {
			for (int j = 0; j < index - 1; j++) {
				i = i.next;
			}
			if (i.next == null) {
				if (tail != null) {
					tail.next = newNode;
					newNode.pre = tail;
					tail = newNode;
				} else {
					head.next = newNode;
					newNode.pre = head;
					tail = newNode;
				}
			} else {
				newNode.next = i.next;
				i.next.pre = newNode;
				newNode.pre = i;
				i.next = newNode;
			 }
		  }
		size++;
		}

	@Override
	public void add(Object element) {
		// TODO Auto-generated method stub

		if (element == null){
			throw null;
		}
		

			doubleListNode newNode = new doubleListNode();
			
			newNode.value=element;
			
			
			if(head == null){
				head=newNode;
			}
			else{
				if(tail == null){
					head.next=newNode;
					newNode.pre=head;
					
					tail=newNode;
				}
				else{
			
			
			
			tail.next=newNode;
		
			newNode.pre=tail;
			tail=newNode;
			}
			}
			size++;
	}

	@Override
	public Object getlast() {
		// TODO Auto-generated method stub
		if (isEmpty()){
			 return null;
		 }
		else if(head.next == null){
			return head.value;
		}
		else{
		return tail.value;
		}
	}

	
	@Override
	public void clear() {
		// TODO Auto-generated method stub

		if (head.next == null){
			head = null;
		}
		else {
			head = null;
			
			tail = null;
		}
		size=0;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if (head == null) {
			return true;
		}
		return false;
	}

	@Override
	public void removelast() {
		// TODO Auto-generated method stub

		if (isEmpty()){
			throw null;
		}
		else if(head.next == null){
			head = null;
			tail = null;
		}
		else if(head.next.next == null){
			head.next = null;
			tail = null;
		}
		else{
			tail = tail.pre;
			tail.next = null;
		}
		size--;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		
			return size;
		
	}

	

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub

		if (isEmpty()){
			return false;
		}
		else{
		doubleListNode i = head;

		while (i != null) {
			if (i.value == o) {

				return true;

			}
			i = i.next;
		}

		return false;

		}
	}

}
