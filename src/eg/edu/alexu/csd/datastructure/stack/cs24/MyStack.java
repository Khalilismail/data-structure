package eg.edu.alexu.csd.datastructure.stack.cs24;

import eg.edu.alexu.csd.datastructure.stack.IStack;

public class MyStack implements IStack {

	RuntimeException e = new RuntimeException();
	
	private Linked h = new Linked();
	@Override
	public void add(int index, Object element) {
		// TODO Auto-generated method stub

		h.add(index, element);
	}

	@Override
	public Object pop() {
		// TODO Auto-generated method stub

		Object r ;
		if(h.size() > 0){
			r = h.getlast();
			h.removelast();
			return r;
			
		}
		else {
			throw e;
		}

	}

	@Override
	public Object peek() {
		// TODO Auto-generated method stub
		
		Object r ;
		if(h.size() > 0){
			r = h.getlast();
	
			return r;
			
		}
		else {
			throw e;
		}
	}

	@Override
	public void push(Object element) {
		// TODO Auto-generated method stub

		h.add(element);
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if(h.isEmpty()){
			return true; 
		}
		else{
		return false;
		}
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return h.size();
	}

}
