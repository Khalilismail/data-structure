package eg.edu.alexu.csd.datastructure.stack.cs24;

import eg.edu.alexu.csd.datastructure.stack.IExpressionEvaluator;

import eg.edu.alexu.csd.datastructure.stack.IStack;

public class App implements IExpressionEvaluator {

	RuntimeException e = new RuntimeException();

	private MyStack m = new MyStack();
	private MyStack s = new MyStack();
	@Override
	public String infixToPostfix(String expression) {
		// TODO Auto-generated method stub
		StringBuilder r = new StringBuilder();
		int x = expression.length();
		String y = expression;
	
		StringBuilder p = new StringBuilder(expression.length());
		for(int i=0;i<x;i++){
			if(y.charAt(i)!=' '){
				p.append(y.charAt(i));
			}
		}
	
		int z = p.length();
		for(int i=0;i<z-1;i++){
			if(p.charAt(i)=='+' || p.charAt(i)=='-' ||
				p.charAt(i)=='*' || p.charAt(i)=='/'){
				if(p.charAt(i+1)=='+' || p.charAt(i+1)=='-' ||
					p.charAt(i+1)=='*' || p.charAt(i+1)=='/'){
					throw e;
				}
			}
		}
		int c1=0,c2=0;
		for(int i=0;i<z;i++){
			if(p.charAt(i)=='('){
				c1++;
			}
			if(p.charAt(i)==')'){
				c2++;
			}
		}
		if(c1 != c2){
			throw e;
		}
		if(y == null || x == 0 || y.charAt(0)=='+' || y.charAt(0)=='-' 
		   ||y.charAt(0)=='*' || y.charAt(0)=='/'|| y.charAt(0)==')' 
		   || y.charAt(x-1)=='+' || y.charAt(x-1)=='-' || y.charAt(x-1)=='*'
		   || y.charAt(x-1)=='/'){
			throw e;
		}
	
		for(int i=0;i<x;i++){
			
			if(y.charAt(i) != ' '){
				
				if(y.charAt(i)!='+' && y.charAt(i)!='-' &&
				   y.charAt(i)!='*' && y.charAt(i)!='/' &&
				   y.charAt(i)!='(' && y.charAt(i)!=')'){
					r.append(y.charAt(i));
					r.append(' ');
					
				}
				else{
					if(m.isEmpty()){
						m.push(y.charAt(i));
					
					}
					
					else{
						
						if(y.charAt(i)=='('){
					
							m.push(y.charAt(i));
						}
						else if(m.peek().equals('(')&&y.charAt(i)!=')'){
							m.push(y.charAt(i));
						}
						else if((m.peek().equals('+') || m.peek().equals('-'))&&
							    (y.charAt(i)=='*'||y.charAt(i)=='/')){
							
							m.push(y.charAt(i));
							
						}
						else if((m.peek().equals('*')|| m.peek().equals('/'))&&
								(y.charAt(i)=='*'||y.charAt(i)=='/')){
							while(!m.isEmpty()){
								if(m.peek().equals('(') || m.peek().equals('+')
										|| m.peek().equals('-')){
									break;
								}
								else{
								r.append(m.pop());
								r.append(' ');
								}
							}
							m.push(y.charAt(i));
							
						}
						else if(y.charAt(i)=='+'||y.charAt(i)=='-'){
							while(!m.isEmpty()){
								if(m.peek().equals('(')){
									
									break;
								}
								else{
								r.append(m.pop());
								r.append(' ');
								}
							}
							m.push(y.charAt(i));
							
						}
						else if(y.charAt(i)==')'){	
							while(!m.isEmpty()){
						if(m.peek().equals('(')){
							m.pop();
							break;
						}
						else{
								r.append(m.pop());
								r.append(' ');
						}
							}
						
						}
					}
				}
			}
		}
		
		while(!m.isEmpty()){
			
		
		r.append(m.pop());
		r.append(' ');
		}
	int f = r.length();
	r.deleteCharAt(f-1);
		
		return String.valueOf(r);
	}

	@Override
	public int evaluate(String expression) {
		// TODO Auto-generated method stub
		int n1,n2,sum=0,sum3;
		char n11,n22,sum1,sum2;
		int x = expression.length();
		String y = expression;
		for(int i=0;i<x;i++){
			if(y.charAt(i) != ' '){
				if(y.charAt(i)>57){
					throw e;
				}
				
				if(y.charAt(i)<48){
					if(y.charAt(i)!=42 && y.charAt(i)!=43 &&
						y.charAt(i)!=45 && y.charAt(i)!=47){
						throw e;
					}
				}
			}
		}
		if(y == null || x == 0){
			throw e;
		}
		for(int i=0;i<x;i++){
			if(y.charAt(i) != ' '){
				if(y.charAt(i)!='+' && y.charAt(i)!='-' &&
				   y.charAt(i)!='*' && y.charAt(i)!='/'){
							s.push(y.charAt(i));
							
						}
				else{
					if(s.peek()==null){
						throw e;
					}
					n22 = (char)s.pop();
					n2 = n22-48;
					if(s.peek()==null){
						throw e;
					}
					n11 = (char)s.pop();
					n1 = n11 - 48;
					if(y.charAt(i)=='+'){
						sum = n1 + n2;
					}
					else if(y.charAt(i)=='-'){
						sum = n1 - n2;
					}
					else if(y.charAt(i)=='*'){
						sum = n1 * n2;
					}
					else if(y.charAt(i)=='/'){
						sum = n1 / n2;
					}
					sum1 = (char)(sum+48);
					s.push(sum1);
				}
			}
		}
		if(s.size()==0){
			throw e;
		}
		else{
		if(s.size()==1){
			sum2 = (char)s.pop();
			sum3 = sum2 - 48;
			return sum3;
		}
		else{
		return 0;
		}
		
		}
	}

}
