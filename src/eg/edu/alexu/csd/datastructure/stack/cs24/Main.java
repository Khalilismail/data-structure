package eg.edu.alexu.csd.datastructure.stack.cs24;

import eg.edu.alexu.csd.datastructure.stack.IExpressionEvaluator;
import eg.edu.alexu.csd.datastructure.stack.IStack;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		IStack m = new MyStack();
		IExpressionEvaluator h = new App();
		
		String f = "(a / b - c + d * e - a * c)";
		System.out.println(h.infixToPostfix(f));
		System.out.println(h.evaluate("5 4 + 2 +"));
		
	}

}
